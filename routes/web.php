<?php

Route::domain(env('APP_DOMAIN'))
    ->namespace('Admin')
    ->group(function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');
        Route::post('logout', 'LoginController@logout')->name('logout');

        Route::middleware('auth')
            ->group(function () {
                Route::get('/', function () {
                    return redirect()->route('users.index');
                });

                Route::get('dashboard', 'UserController@index')->name('dashboard');

                Route::get('users', 'UserController@index')->name('users.index');
                Route::get('users/columns', 'UserController@columns');
                Route::get('users/tables', 'UserController@tables');
                Route::get('users/record/{user}', 'UserController@record');
                Route::post('users', 'UserController@store');
                Route::get('users/records', 'UserController@records');
                Route::delete('users/{user}', 'UserController@destroy');
            });
    });
