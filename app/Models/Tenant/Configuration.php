<?php

namespace App\Models\Tenant;

use Hyn\Tenancy\Abstracts\TenantModel;

class Configuration extends TenantModel
{
    protected $fillable = ['send_auto', 'cron'];
}
