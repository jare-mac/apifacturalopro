<?php
namespace App\Models\Tenant\Catalogs;

use Hyn\Tenancy\Abstracts\TenantModel;

class ModelCatalog extends TenantModel
{
    public function scopeWhereActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOrderByDescription($query)
    {
        return $query->orderBy('description');
    }
}
