<?php

namespace App\Models\Tenant\Catalogs;

class LegendType extends ModelCatalog
{
    protected $table = "cat_legend_types";
    public $incrementing = false;
}
