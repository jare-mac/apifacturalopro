<?php

namespace App\Models\Tenant\Catalogs;

class TransferReasonType extends ModelCatalog
{
    protected $table = "cat_transfer_reason_types";
    public $incrementing = false;
}
