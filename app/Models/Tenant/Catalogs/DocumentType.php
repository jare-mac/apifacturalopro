<?php

namespace App\Models\Tenant\Catalogs;

class DocumentType extends ModelCatalog
{
    protected $table = "cat_document_types";
    public $incrementing = false;
}
