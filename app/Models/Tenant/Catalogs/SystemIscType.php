<?php

namespace App\Models\Tenant\Catalogs;

class SystemIscType extends ModelCatalog
{
    protected $table = "cat_system_isc_types";
    public $incrementing = false;
}
