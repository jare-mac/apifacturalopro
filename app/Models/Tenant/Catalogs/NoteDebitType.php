<?php

namespace App\Models\Tenant\Catalogs;

class NoteDebitType extends ModelCatalog
{
    protected $table = "cat_note_debit_types";
    public $incrementing = false;
}
