<?php

namespace App\Models\Tenant\Catalogs;

class RelatedTaxDocumentType extends ModelCatalog
{
    protected $table = "cat_related_tax_document_types";
    public $incrementing = false;
}
