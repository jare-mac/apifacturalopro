<?php

namespace App\Models\Tenant;

use Hyn\Tenancy\Abstracts\TenantModel;

class Group extends TenantModel
{
    public $timestamps = false;
}
