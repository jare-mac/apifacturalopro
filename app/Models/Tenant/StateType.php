<?php

namespace App\Models\Tenant;

use Hyn\Tenancy\Abstracts\TenantModel;

class StateType extends TenantModel
{
    public $incrementing = false;
    public $timestamps = false;
}
