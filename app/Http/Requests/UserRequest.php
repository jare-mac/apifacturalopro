<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required'],
            'email' => ['required'],
            'password' => ['min:6', 'confirmed'],
            'fqdn' => ['required', 'string']
        ];
    }

    public function attributes()
    {
        return [
            'fqdn' => 'Subdomain',
        ];
    }

    protected function prepareForValidation()
    {
        if ($this->has('fqdn') && $this->fqdn != "" ) {
            $fqdn = sprintf("%s.%s", $this->fqdn, env('APP_DOMAIN'));
            $this->merge(['fqdn'=>$fqdn]);
        }
    }
}
