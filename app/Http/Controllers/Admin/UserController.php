<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Environment;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Models\Admin\User;
use App\Http\Resources\UserCollection;
use App\Models\Tenant\User as TenantUser;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }

    public function columns()
    {
        return ['name' => 'Nombre'];
    }

    public function records(Request $request)
    {
        $records = User::where($request->column, 'like', "%{$request->value}%")
            ->where('role', '<>', 'admin')
            ->orderBy('name');

        return UserResource::collection($records->paginate(env('ITEMS_PER_PAGE', 5)));
    }

    public function record($id)
    {
        $record = new UserResource(User::findOrFail($id));

        return $record;
    }

    public function store(UserRequest $request)
    {
        $id = $request->input('id');
        $fqdn = $request->input('fqdn');
        $password = $request->input('password');
        $api_token = str_random(50);

        $user = User::firstOrNew(['id' => $id]);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = 'user';

        if ($user->id) {
            if ($password != "") {
                $user->password = bcrypt($password);
            }

            // Get tenant belong to the current user
            $hostname = Hostname::find($user->hostname_id);
            $website = Website::find($user->hostname->website_id);
            $tenancy = app(Environment::class);
            $tenancy->hostname($hostname);
            $tenancy->tenant($website);
        } else {
            $user->api_token = $api_token;
            $user->password = bcrypt($password);

            // Create new tenant
            $tenancy = $this->createTenant($fqdn);

            $user->hostname_id = $tenancy->hostname()->id;
        }

        $user->save();
        $this->createOrUpdateTenantUser($user);

        return [
            'success' => true,
            'message' => ($id)?'Usuario actualizado':'Usuario registrado'
        ];
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $hostname = Hostname::find($user->hostname_id);
        $website = Website::find($user->hostname->website_id);
        app(HostnameRepository::class)->delete($hostname, true);
        app(WebsiteRepository::class)->delete($website, true);

        $hostname->delete();
        $website->delete();
        $user->delete();

        return [
            'success' => true,
            'message' => 'Usuario eliminado con éxito'
        ];
    }

    public function createOrUpdateTenantUser($user)
    {
        $tenantUser = TenantUser::firstOrNew(['email' => $user->email]);
        $tenantUser->name = $user->name;
        $tenantUser->email = $user->email;
        $tenantUser->api_token = $user->api_token;
        $tenantUser->password = $user->password;
        $tenantUser->role = 'user';
        $tenantUser->save();
    }

    public function createTenant($fqdn)
    {
        $website = new Website();
        $website->uuid = "fac_" . strtolower(str_random(10));
        app(WebsiteRepository::class)->create($website);

        $hostname = new Hostname();
        $hostname->fqdn = sprintf("%s.%s", $fqdn, env('APP_DOMAIN'));
        $hostname = app(HostnameRepository::class)->create($hostname);
        app(HostnameRepository::class)->attach($hostname, $website);

        $tenancy = app(Environment::class);
        $tenancy->hostname($hostname);
        $tenancy->tenant($website);

        return $tenancy;
    }
}
