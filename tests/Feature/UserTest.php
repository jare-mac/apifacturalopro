<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    public function testLoginFalse()
    {
        $credential = [
            'email' => 'test@gmail.com',
            'password' => 'incorrectpass'
        ];

        $response = $this->post('login', $credential);

        $response->assertSessionHasErrors();
    }

    public function testLoginTrue()
    {
        $credential = [
            'email' => 'admin@gmail.com',
            'password' => '123456'
        ];

        $response = $this->post('login', $credential);
        //var_dump($response);
        $response->assertSessionMissing('errors');
    }
}
